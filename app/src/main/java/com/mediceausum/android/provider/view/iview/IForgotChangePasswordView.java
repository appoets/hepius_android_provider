package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.presenter.ipresenter.IForgotChangePasswordPresenter;

public interface IForgotChangePasswordView extends IView<IForgotChangePasswordPresenter> {
                void goToLogin();
}
