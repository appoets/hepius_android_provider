package com.mediceausum.android.provider.view.adapter.listener;


import com.mediceausum.android.provider.model.dto.response.ScheduledListResponse;

public interface IScheduledListListener extends BaseRecyclerListener<ScheduledListResponse> {
}
