package com.mediceausum.android.provider.presenter.ipresenter;

public interface IScheduledListPresenter extends IPresenter {
    void getScheduledList();
}
