package com.mediceausum.android.provider.presenter.ipresenter;

import com.mediceausum.android.provider.model.dto.request.LoginRequest;
import com.mediceausum.android.provider.model.dto.request.RegisterRequest;

public interface IRegisterPresenter extends IPresenter {
    void goToLogin();
    void getServiceData();
    void postLogin(LoginRequest request);
    void postRegister(RegisterRequest registerRequest);
}