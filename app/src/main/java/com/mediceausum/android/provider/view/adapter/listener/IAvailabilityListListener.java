package com.mediceausum.android.provider.view.adapter.listener;


import com.mediceausum.android.provider.model.dto.response.AvailabilityListResponse;

public interface IAvailabilityListListener extends BaseRecyclerListener<AvailabilityListResponse> {
}
