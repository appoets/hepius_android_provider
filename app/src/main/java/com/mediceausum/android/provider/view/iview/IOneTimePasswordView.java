package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.presenter.ipresenter.IOneTimePasswordPresenter;

public interface IOneTimePasswordView extends IView<IOneTimePasswordPresenter> {
        void goToForgotChangePassword();
        void setUp();
}
