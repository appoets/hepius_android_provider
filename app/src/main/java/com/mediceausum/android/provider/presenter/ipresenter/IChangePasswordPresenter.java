package com.mediceausum.android.provider.presenter.ipresenter;

import com.mediceausum.android.provider.model.dto.request.PasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void updatePassword(PasswordRequest request);
    void goToLogin();
}