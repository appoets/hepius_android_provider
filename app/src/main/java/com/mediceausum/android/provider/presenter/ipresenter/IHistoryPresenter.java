package com.mediceausum.android.provider.presenter.ipresenter;

public interface IHistoryPresenter extends IPresenter {
        void getHistory();
}