package com.mediceausum.android.provider.presenter;

import com.mediceausum.android.provider.model.CustomException;
import com.mediceausum.android.provider.model.ForgotPasswordModel;
import com.mediceausum.android.provider.model.dto.response.ForgotPasswordResponse;
import com.mediceausum.android.provider.model.listener.IModelListener;
import com.mediceausum.android.provider.presenter.ipresenter.IForgotPasswordPresenter;
import com.mediceausum.android.provider.view.iview.IForgotPasswordView;

import org.jetbrains.annotations.NotNull;

import static com.mediceausum.android.provider.MyApplication.getApplicationInstance;


public class ForgotPasswordPresenter extends BasePresenter<IForgotPasswordView> implements IForgotPasswordPresenter {


    @Override
    public void goToOneTimePassword() {
        iView.goToOneTimePassword();
    }

    public ForgotPasswordPresenter(IForgotPasswordView iView) {
        super(iView);
    }

    @Override
    public void getOTPDetails(String email){
        iView.showProgressbar();
        new ForgotPasswordModel(new IModelListener<ForgotPasswordResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ForgotPasswordResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setOTP(response.getProvider().getOtp()+"");
                iView.goToOneTimePassword();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getOTPDetails(email);
    }
}
