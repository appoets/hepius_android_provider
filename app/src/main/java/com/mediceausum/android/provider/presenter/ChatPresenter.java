package com.mediceausum.android.provider.presenter;

import android.os.Bundle;

import com.mediceausum.android.provider.presenter.ipresenter.IChatPresenter;
import com.mediceausum.android.provider.view.iview.IChatView;


public class ChatPresenter extends BasePresenter<IChatView> implements IChatPresenter {

    public ChatPresenter(IChatView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }
}
