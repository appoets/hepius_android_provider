package com.mediceausum.android.provider.presenter.ipresenter;

import com.mediceausum.android.provider.model.dto.request.ScheduleRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface ISchedulePresenter {
    void updateSchedule(ScheduleRequest request);
    void showCalendarDialog();
    void showTimePickerDialog();
}
