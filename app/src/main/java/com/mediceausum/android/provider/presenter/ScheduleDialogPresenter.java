package com.mediceausum.android.provider.presenter;

import com.mediceausum.android.provider.presenter.ipresenter.IScheduleDialogPresenter;
import com.mediceausum.android.provider.view.iview.IScheduleDialogView;


public class ScheduleDialogPresenter extends BasePresenter<IScheduleDialogView> implements IScheduleDialogPresenter {


    public ScheduleDialogPresenter(IScheduleDialogView iView) {
        super(iView);
    }

}
