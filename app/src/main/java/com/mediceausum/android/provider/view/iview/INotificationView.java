package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.model.dto.response.Provider;
import com.mediceausum.android.provider.presenter.ipresenter.INotificationPresenter;
import com.mediceausum.android.provider.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
    void showNotificationDialog(int pos, Provider data);
}
