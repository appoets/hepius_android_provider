package com.mediceausum.android.provider.view.iview;


import com.mediceausum.android.provider.presenter.ipresenter.IChatPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IChatView extends IView<IChatPresenter> {
        void setUp();
}
