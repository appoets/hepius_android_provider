package com.mediceausum.android.provider.view.adapter.listener;

import com.mediceausum.android.provider.model.dto.common.HistoryItem;

public interface IHistoryRecyclerListener extends BaseRecyclerListener<HistoryItem> {
}
