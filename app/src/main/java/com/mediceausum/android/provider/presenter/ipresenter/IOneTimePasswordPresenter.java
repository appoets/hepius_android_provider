package com.mediceausum.android.provider.presenter.ipresenter;

public interface IOneTimePasswordPresenter extends IPresenter {
    void goToForgotChangePassword();
}