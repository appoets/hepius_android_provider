package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
