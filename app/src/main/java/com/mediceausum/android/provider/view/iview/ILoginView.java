package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.presenter.ipresenter.ILoginPresenter;

public interface ILoginView extends IView<ILoginPresenter> {
    void goToRegistration();
    void goToForgotPassword();
    void goToHome();
}
