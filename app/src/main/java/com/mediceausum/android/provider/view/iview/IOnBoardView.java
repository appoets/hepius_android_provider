package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.presenter.ipresenter.IOnBoardPresenter;

public interface IOnBoardView extends IView<IOnBoardPresenter> {

    void initSetUp();

    void gotoLogin();
}